/*
 * KWin XRDesktop Plugin
 * Copyright 2018 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "vrmirror_config.h"
#include <kwineffects_interface.h>

#include <QAction>

#include <KGlobalAccel>
#include <KLocalizedString>
#include <KActionCollection>
#include <KShortcutsEditor>
#include <KAboutData>
#include <KPluginFactory>

#include <QVBoxLayout>

K_PLUGIN_FACTORY_WITH_JSON(VRMirrorEffectConfigFactory,
                           "vrmirror_config.json",
                           registerPlugin<KWin::VRMirrorEffectConfig>();)

namespace KWin
{

VRMirrorEffectConfig::VRMirrorEffectConfig(QWidget *parent, const QVariantList &args) :
    KCModule(KAboutData::pluginData(QStringLiteral("xrdesktop")), parent, args)
{
    QVBoxLayout *layout = new QVBoxLayout(this);

    auto *actionCollection = new KActionCollection(this, QStringLiteral("kwin"));

    actionCollection->setComponentDisplayName(i18n("KWin"));
    actionCollection->setConfigGroup(QStringLiteral("xrdesktop"));
    actionCollection->setConfigGlobal(true);

    QAction *toggleAction = actionCollection->addAction(QStringLiteral("Toggle"));
    toggleAction->setText(i18n("Toggle Mirroring Windows to VR with xrdesktop"));
    toggleAction->setProperty("isConfigurationAction", true);
    KGlobalAccel::self()->setDefaultShortcut(toggleAction, {});
    KGlobalAccel::self()->setShortcut(toggleAction, {});

    mShortcutEditor = new KShortcutsEditor(actionCollection, this,
                                           KShortcutsEditor::GlobalAction, KShortcutsEditor::LetterShortcutsDisallowed);
    connect(mShortcutEditor, SIGNAL(keyChange()), this, SLOT(changed()));
    layout->addWidget(mShortcutEditor);

    load();
}

VRMirrorEffectConfig::~VRMirrorEffectConfig()
{
    // Undo (only) unsaved changes to global key shortcuts
    mShortcutEditor->undoChanges();
}

void VRMirrorEffectConfig::load()
{
    KCModule::load();

    emit changed(false);
}

void VRMirrorEffectConfig::save()
{
    KCModule::save();

    mShortcutEditor->save();    // undo() will restore to this state from now on

    emit changed(false);
    OrgKdeKwinEffectsInterface interface(QStringLiteral("org.kde.KWin"),
                                             QStringLiteral("/Effects"),
                                             QDBusConnection::sessionBus());
    interface.reconfigureEffect(QStringLiteral("xrdesktop"));
}

void VRMirrorEffectConfig::defaults()
{
    mShortcutEditor->allDefault();

    emit changed(true);
}

} // namespace

#include "vrmirror_config.moc"
